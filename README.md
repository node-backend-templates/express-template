# NOTA

Se esta trabajando en una version 2 del template la cual se basara en arquitectura Event driven, incluira routing-controllers y TypeOrm; la arquitectura hexagonal, screaming y vertical slicing se mantendran, pero es posible que la version 2 sea muy diferente a la version actual.

## Introducción a Arquitectura Hexagonal, Screaming y Vertical Slicing

La Arquitectura Hexagonal, también conocida como Arquitectura de Puertos y Adaptadores, es un enfoque arquitectónico que busca separar la lógica de negocio central de una aplicación de los detalles técnicos y de infraestructura. Este enfoque promueve la modularidad, la reutilización y la facilidad de prueba al aislar las capas de la aplicación y definir claramente las dependencias.

El principio de "Screaming Architecture" (Arquitectura que Grita) se refiere a una arquitectura que hace evidente la intención y la lógica de negocio de la aplicación. Al observar la estructura de la aplicación, se debería poder comprender fácilmente qué es lo que la aplicación hace y cómo lo hace.

Vertical Slicing, por otro lado, es una técnica para organizar el código de una aplicación en función de las características y los flujos de trabajo que ofrece, en lugar de organizarlo por capas técnicas. Esto facilita el mantenimiento y la evolución de la aplicación, ya que los cambios se pueden realizar en una sola área de la aplicación sin afectar otras partes.

En conjunto, la Arquitectura Hexagonal, el Screaming Architecture y el Vertical Slicing promueven un código más limpio, modular y fácilmente comprensible.

## Inversify

Inversify es una poderosa biblioteca de inversión de control (IoC) y contenedor de inyección de dependencias (DI) para JavaScript y TypeScript. Proporciona una forma fácil de administrar las dependencias en una aplicación y promueve un diseño modular y desacoplado.

### Características principales

- **Inyección de dependencias**: Inversify permite definir y resolver dependencias entre diferentes componentes de una aplicación de manera declarativa. Esto ayuda a reducir el acoplamiento y facilita la reutilización y el mantenimiento del código.

- **Contenedor ligero**: Inversify proporciona un contenedor liviano que actúa como un almacén central para definir y resolver dependencias. El contenedor es fácil de configurar y se puede personalizar para adaptarse a las necesidades específicas de una aplicación.

- **Decoradores y anotaciones**: La biblioteca utiliza decoradores y anotaciones para definir y marcar las dependencias en el código. Esto mejora la legibilidad y facilita la comprensión de las relaciones entre los componentes.

- **Soporte para TypeScript**: Inversify está diseñado para funcionar de manera nativa con TypeScript, aprovechando las características del lenguaje como los tipos estáticos y los decoradores. Esto ayuda a detectar errores de tiempo de compilación y proporciona una experiencia de desarrollo más sólida.

## Inversify-express-utils

Inversify-express-utils es una extensión de Inversify que proporciona un conjunto de utilidades para construir aplicaciones web utilizando Express.js y Inversify. Combina los beneficios de ambos frameworks, facilitando la creación de API RESTful robustas y bien estructuradas.

### Características principales

- **Controladores basados en clases**: Inversify-express-utils permite definir controladores utilizando clases, lo que ayuda a organizar el código de manera lógica y modular. Cada controlador se puede inyectar con las dependencias necesarias y ofrece una forma clara de definir rutas y acciones correspondientes.

- **Decoradores de enrutamiento**: La biblioteca utiliza decoradores para definir las rutas y los métodos HTTP correspondientes en cada controlador. Esto simplifica la configuración de las rutas y mejora la legibilidad del código.

- **Middlewares**: Inversify-express-utils proporciona soporte para la inclusión de middlewares en cada controlador o enrutador. Esto permite agregar funcionalidades adicionales, como autenticación, validación de datos, manejo de errores, etc., en puntos específicos del flujo de solicitud.

- **Integración con inversify**: Inversify-express-utils se integra perfectamente con la biblioteca Inversify, lo que permite la inyección de dependencias en los controladores y otras clases relacionadas. Esto facilita la gestión de las dependencias y promueve un diseño desacoplado y fácilmente testeable.

## Pasos para ejecutar los comandos npm

1. Asegúrate de tener Node.js y npm instalados en tu sistema.

para instalar todas las dependencias ejecuta:

   ```shell
      npm i o npm install
   ```

2. Abre una terminal o línea de comandos en el directorio raíz de tu proyecto.

3. Ejecuta el siguiente comando para iniciar la aplicación en modo de producción:

   ```shell
   npm start
   ```

   Este comando ejecutará el archivo `dist/index.js` utilizando Node.js.

4. Para iniciar la aplicación en modo de desarrollo, ejecuta el siguiente comando:

   ```shell
   npm run start:dev
   ```

   Este comando utiliza `ts-node` para ejecutar directamente el archivo `index.ts`, que es útil durante el desarrollo.

5. Si deseas compilar el código TypeScript en JavaScript, puedes ejecutar el siguiente comando:

   ```shell
   npm run build
   ```

   Este comando utiliza `tsc` (TypeScript Compiler) para compilar los archivos TypeScript y generar el código JavaScript en la carpeta `dist`.

6. Para ejecutar el linter y verificar el estilo del código, utiliza el siguiente comando:

   ```shell
   npm run lint
   ```

   Este comando ejecuta `eslint` para analizar el código y mostrar los posibles errores o problemas de estilo.

7. Si deseas corregir automáticamente los problemas de estilo detectados por el linter, ejecuta el siguiente comando:

   ```shell
   npm run lint:fix
   ```

   Este comando utiliza `eslint` con la opción `--fix` para corregir automáticamente los problemas de estilo en los archivos.

8. Por último, para ejecutar las pruebas y generar el informe de cobertura de código, utiliza el siguiente comando:

   ```shell
   npm test
   ```

   Este comando utiliza `jest` para ejecutar las pruebas y mostrar los resultados, incluyendo la cobertura de código.

Recuerda que estos comandos deben ser ejecutados en el directorio raíz de tu proyecto, y es posible que necesites tener las instaladas.
