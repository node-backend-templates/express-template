import "dotenv/config";

export enum DIALECT {
	MYSQL = "mysql",
	POSTGRES = "postgres",
	SQLITE = "sqlite",
	MARIADB = "mariadb",
	MSSQL = "mssql",
	DB2 = "db2",
	SNOWFLAKE = "snowflake",
	ORACLE = "oracle",
}

const databasePort = process.env.DB_PORT ?? "3306";
export const constants = {
	database: {
		DATABASE: process.env.DATABASE ?? undefined,
		PASSWORD: process.env.PASSWORD ?? undefined,
		USER: process.env.USER ?? "root",
		DB_PORT: parseInt(databasePort, 10),
	},
	env: process.env.NODE_ENV ?? "dev",
	port: process.env.PORT ?? 8080,
};
