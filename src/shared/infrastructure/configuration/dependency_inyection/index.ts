import { Container } from "inversify";

import userDependencies from "../../../../user/infrastructure/configuration/dependency-injection/UserDependencies";

export const container = new Container();

export const configDependencies = (): void => {
	userDependencies();
};
