import { Sequelize } from "sequelize-typescript";

import { MySqlUserSchema } from "../../../../../user/infrastructure/driven/persistence/schemas/MySqlUserSchema";
import { constants, DIALECT } from "../../constants/constants";

export const sequelize = new Sequelize({
	database: constants.database.DATABASE,
	dialect: DIALECT.MYSQL,
	username: "root",
	password: constants.database.PASSWORD,
	port: constants.database.DB_PORT,
});

export const sequelizeConnect = async (): Promise<void> => {
	try {
		sequelize.addModels([MySqlUserSchema]);
		await sequelize.sync({
			alter: true,
		});
		await sequelize.authenticate();
	} catch (error) {
		console.error(error);
	}
};
