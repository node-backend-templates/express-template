import "reflect-metadata";
import "dotenv/config";
import "./shared/infrastructure/configuration/dependency_inyection/dependency-injection-controllers";

import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import { InversifyExpressServer } from "inversify-express-utils";
import morgan from "morgan";

import { constants } from "./shared/infrastructure/configuration/constants/constants";
import { sequelizeConnect } from "./shared/infrastructure/configuration/databases/sequelize/sequelize";
import {
	configDependencies,
	container,
} from "./shared/infrastructure/configuration/dependency_inyection";

class App {
	private readonly port = constants.port;

	constructor() {
		configDependencies();
		this.createServer();
		sequelizeConnect()
			.then()
			.catch((error) => console.error(error));
	}

	createServer(): void {
		const server: InversifyExpressServer = new InversifyExpressServer(container);

		server.setConfig((app) => {
			app.use(helmet());
			app.use(morgan(constants.env));
			app.use(cors());
			app.use(bodyParser.json());
			app.use(bodyParser.urlencoded({ extended: true }));
		});

		const app = server.build();
		app.listen(this.port);
	}
}

export default new App();
