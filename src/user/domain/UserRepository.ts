import { User } from "./User";

export interface UserRepository {
	save(user: User): Promise<void>;
	findById(id: string): Promise<User | null>;
	findByName(name: string): Promise<User[]>;
	findAll(): Promise<User[]>;
}
