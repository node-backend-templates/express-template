import { validate } from "uuid";

import { UUID_NOT_VALID } from "../../shared/domain/exceptions/UuidNotValid";

export class UserId {
	value: string;

	constructor(id: string) {
		this.validateUuid(id);
		this.value = id;
	}

	private validateUuid(id: string): void | Error {
		if (!validate(id)) {
			throw new UUID_NOT_VALID("El id del usuario no es valido");
		}
	}
}
