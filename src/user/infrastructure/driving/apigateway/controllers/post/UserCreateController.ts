import express from "express";
import httpStatus from "http-status";
import { inject } from "inversify";
import {
	BaseHttpController,
	controller,
	httpPost,
	request,
	response,
} from "inversify-express-utils";

import { UserCreate } from "../../../../../application/services/create/UserCreate";
import { User } from "../../../../../domain/User";
import Types from "../../../../configuration/dependency-injection/Types";

@controller("/users")
export class UserCreateController extends BaseHttpController {
	constructor(@inject(Types.UserCreate) private readonly userService: UserCreate) {
		super();
	}

	@httpPost("/")
	private async create(
		@request() req: express.Request<unknown, unknown, User>,
		@response() _res: express.Response
	) {
		try {
			const user: User = req.body;
			await this.userService.run(user);

			return this.ok();
		} catch (error) {
			return this.json(error, httpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
