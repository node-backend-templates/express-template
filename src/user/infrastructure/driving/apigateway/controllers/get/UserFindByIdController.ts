import express from "express";
import httpStatus from "http-status";
import { inject } from "inversify";
import {
	BaseHttpController,
	controller,
	httpGet,
	interfaces,
	request,
	requestParam,
} from "inversify-express-utils";

import { UserFindById } from "../../../../../application/services/find/UserFindById";
import Types from "../../../../configuration/dependency-injection/Types";

@controller("/users")
export class UserFindByIdController extends BaseHttpController implements interfaces.Controller {
	constructor(@inject(Types.UserFindById) private readonly userService: UserFindById) {
		super();
	}

	@httpGet("/id/:id")
	private async findById(@requestParam("id") id: string, @request() _res: express.Response) {
		try {
			const data = await this.userService.run(id);

			return this.json(data, httpStatus.OK);
		} catch (error) {
			return this.json(error, httpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
