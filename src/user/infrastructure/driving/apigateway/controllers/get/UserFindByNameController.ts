import express from "express";
import httpStatus from "http-status";
import { inject } from "inversify";
import {
	BaseHttpController,
	controller,
	httpGet,
	interfaces,
	request,
	requestParam,
} from "inversify-express-utils";

import { UserFindByName } from "../../../../../application/services/find/UserFindByName";
import Types from "../../../../configuration/dependency-injection/Types";

@controller("/users")
export class UserFindByNameController extends BaseHttpController implements interfaces.Controller {
	constructor(@inject(Types.UserFindByName) private readonly userService: UserFindByName) {
		super();
	}

	@httpGet("/:name")
	private async findByName(@requestParam("name") name: string, @request() _res: express.Response) {
		try {
			const data = await this.userService.run(name);

			return this.json(data, httpStatus.OK);
		} catch (error) {
			return this.json(error, httpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
