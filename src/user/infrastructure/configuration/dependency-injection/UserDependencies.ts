import { container } from "../../../../shared/infrastructure/configuration/dependency_inyection";
import { UserCreate } from "../../../application/services/create/UserCreate";
import { UserFindAll } from "../../../application/services/find/UserFindAll";
import { UserFindById } from "../../../application/services/find/UserFindById";
import { UserFindByName } from "../../../application/services/find/UserFindByName";
import { UserRepository } from "../../../domain/UserRepository";
import { MySqlUserRepository } from "../../driven/persistence/repositories/MySqlUserRepository";
import Types from "./Types";

const userContainer = (): void => {
	container.bind<UserRepository>(Types.UserRepository).to(MySqlUserRepository);
	container.bind<UserFindById>(Types.UserFindById).to(UserFindById);
	container.bind<UserFindByName>(Types.UserFindByName).to(UserFindByName);
	container.bind<UserFindAll>(Types.UserFindAll).to(UserFindAll);
	container.bind<UserCreate>(Types.UserCreate).to(UserCreate);
};

export default userContainer;
