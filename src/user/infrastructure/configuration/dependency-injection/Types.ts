const Types = {
	UserRepository: Symbol.for("MySqlUserRepository"),
	UserFindAll: Symbol.for("UserFindAll"),
	UserFindById: Symbol.for("UserFindById"),
	UserFindByName: Symbol.for("UserFindByName"),
	UserCreate: Symbol.for("UserCreate"),
};

export default Types;
