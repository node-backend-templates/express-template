import { UUIDV4 } from "sequelize";
import { Column, DataType, Model, Table } from "sequelize-typescript";

@Table({
	timestamps: true,
	tableName: "users",
})
export class MySqlUserSchema extends Model {
	@Column({
		type: DataType.UUID,
		primaryKey: true,
		defaultValue: UUIDV4,
	})
	id!: string;

	@Column({
		type: DataType.TEXT,
		allowNull: false,
	})
	name!: string;
}
