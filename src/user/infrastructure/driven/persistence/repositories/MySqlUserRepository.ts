import { injectable } from "inversify";

import { User } from "../../../../domain/User";
import { UserRepository } from "../../../../domain/UserRepository";
import { MySqlUserSchema } from "../schemas/MySqlUserSchema";

@injectable()
export class MySqlUserRepository implements UserRepository {
	async findAll(): Promise<User[]> {
		return await MySqlUserSchema.findAll();
	}

	async findById(id: string): Promise<User | null> {
		return await MySqlUserSchema.findByPk(id);
	}

	async findByName(name: string): Promise<User[]> {
		return await MySqlUserSchema.findAll({
			where: {
				name,
			},
		});
	}

	async save(user: User): Promise<void> {
		await MySqlUserSchema.create({
			name: user.name,
		});
	}
}
