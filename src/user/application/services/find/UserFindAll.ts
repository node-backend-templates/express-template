import { inject, injectable } from "inversify";

import { User } from "../../../domain/User";
import { UserRepository } from "../../../domain/UserRepository";
import Types from "../../../infrastructure/configuration/dependency-injection/Types";

@injectable()
export class UserFindAll {
	constructor(@inject(Types.UserRepository) private readonly userRepository: UserRepository) {}

	async run(): Promise<User[]> {
		return this.userRepository.findAll();
	}
}
