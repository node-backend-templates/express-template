import { inject, injectable } from "inversify";

import { User } from "../../../domain/User";
import { UserId } from "../../../domain/UserId";
import { UserRepository } from "../../../domain/UserRepository";
import Types from "../../../infrastructure/configuration/dependency-injection/Types";

@injectable()
export class UserFindById {
	constructor(@inject(Types.UserRepository) private readonly userRepository: UserRepository) {}

	async run(id: string): Promise<User | unknown> {
		try {
			const validateId = new UserId(id);

			return await this.userRepository.findById(validateId.value);
		} catch (error) {
			throw new Error("Not valid id");
		}
	}
}
